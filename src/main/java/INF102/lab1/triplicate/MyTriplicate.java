package INF102.lab1.triplicate;

import java.util.HashMap;
import java.util.List;

public class MyTriplicate<T> implements ITriplicate<T> {

    @Override
    public T findTriplicate(List<T> list) {
        HashMap<T,Integer> instances = new HashMap<>();
        for(T element : list) {
            instances.put(element, instances.getOrDefault(element, 0) +1);
            if(instances.get(element).equals(3)) {
                return element;
            }
        }
        return null;
    }
    
}
